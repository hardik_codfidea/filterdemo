package com.codfidea.filterdemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class OneFragment extends Fragment{

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NumberPicker numberPicker = view.findViewById(R.id.horizontal_number_picker);
        NumberPicker numberPicker1 = view.findViewById(R.id.horizontal_number_picker1);
        NumberPicker numberPicker2 = view.findViewById(R.id.horizontal_number_picker2);
        NumberPicker numberPicker3 = view.findViewById(R.id.horizontal_number_picker3);
        NumberPicker numberPicker4 = view.findViewById(R.id.horizontal_number_picker4);
        NumberPicker numberPicker5 = view.findViewById(R.id.horizontal_number_picker5);
        NumberPicker numberPicker6 = view.findViewById(R.id.horizontal_number_picker6);

        // Using string values
// IMPORTANT! setMinValue to 1 and call setDisplayedValues after setMinValue and setMaxValue
        String[] data = {"Man", "Woman"};
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(data.length);
        numberPicker.setDisplayedValues(data);
        numberPicker.setValue(1);


        String[] data1 = {"18", "19","20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};
        numberPicker1.setMinValue(1);
        numberPicker1.setMaxValue(data1.length);
        numberPicker1.setDisplayedValues(data1);
        numberPicker1.setValue(2);

        String[] data2 = {"24", "25", "26", "27", "28", "29", "30","31", "32","33"};
        numberPicker2.setMinValue(1);
        numberPicker2.setMaxValue(data2.length);
        numberPicker2.setDisplayedValues(data2);
        numberPicker2.setValue(9);


        String[] data3 = {"Bangladesh", "Barbados", "Belarus", "Belgium"};
        numberPicker3.setMinValue(1);
        numberPicker3.setMaxValue(data3.length);
        numberPicker3.setDisplayedValues(data3);
        numberPicker3.setValue(2);

        String[] data4 = {"Bathsheba", "Hoteltown", "Oistins", "Minsk", "Mogilev"};
        numberPicker4.setMinValue(1);
        numberPicker4.setMaxValue(data4.length);
        numberPicker4.setDisplayedValues(data4);
        numberPicker4.setValue(2);

        String[] data5 = {"all", "online"};
        numberPicker5.setMinValue(1);
        numberPicker5.setMaxValue(data5.length);
        numberPicker5.setDisplayedValues(data5);
        numberPicker5.setValue(0);

        String[] data6 = {"all", "verified"};
        numberPicker6.setMinValue(1);
        numberPicker6.setMaxValue(data6.length);
        numberPicker6.setDisplayedValues(data6);
        numberPicker6.setValue(0);
    }

}